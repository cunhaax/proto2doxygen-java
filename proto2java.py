#!/usr/bin/env python3

"""Proto to Doxygen (Java) filter

The following conversions are done by the filter:

* message --> class
* required --> java comment
* optional --> java comment
* value types --> according with the official documentation: https://developers.google.com/protocol-buffers/docs/proto#scalar
* oneof test_oneof --> class OneOf_test_oneof aggregating all the oneof attributes
* service --> interface
* extend EndPoint --> class Extend_Filename.proto_EndPoint extends EndPoint
"""

__author__ = "André Cunha"
__license__ = "GPLv3"



import os.path
import re
import sys


def handle_file(filename):
    if re.search('.+\.proto$', filename):
        with open(filename, 'r') as lines:
            process_lines(lines)


def process_types(line):
    res = line

    res = re.sub('double', 'double', res)
    res = re.sub('float', 'float', res)

    res = re.sub('sfixed32', 'int', res)
    res = re.sub('sfixed64', 'long', res)
    res = re.sub('fixed32', 'int', res)
    res = re.sub('fixed64', 'long', res)

    res = re.sub('uint32', 'int', res)
    res = re.sub('uint64', 'long', res)
    res = re.sub('sint32', 'int', res)
    res = re.sub('sint64', 'long', res)

    res = re.sub('int32', 'int', res)
    res = re.sub('int64', 'long', res)

    res = re.sub('bool', 'boolean', res)
    res = re.sub('string', 'String', res)
    res = re.sub('bytes', 'ByteString', res)

    res = re.sub('^\s*repeated', 'List', res)

    return res


def process_lines(lines):
    line_nr = 0
    block_level = 0
    output = ''

    for line in lines:
        line_nr += 1

        # set nested block level
        if re.search('^[\sa-zA-Z0-9\-\_]*\{', line):
            block_level += 1

        if re.search('^[\sa-zA-Z0-9\-\_]*\}', line):
            block_level -= 1

        # make regular comments visible to doxygen
        line = re.sub('//', '///', line)

        line = re.sub('^\s*syntax', '//syntax', line)
        line = re.sub('^\s*option java_package', '//option java_package', line)
        line = re.sub('^\s*import', '//import', line)
        line = re.sub('^\s*message', 'class', line)
        line = re.sub('^\s*required', '/** <b>required</b> */', line)
        line = re.sub('^\s*optional', '/** <b>optional</b> */', line)
        line = re.sub('oneof\ +', 'class OneOf_', line)
        line = re.sub('^\s*extensions', '//extensions', line)

        line = process_types(line)

        # handle service
        line = re.sub('^\s*service', 'interface', line)
        match = re.search('^\s*rpc\ +(.+?\ *\(.*?\))\ *returns\ *\((.+?)\)', line)
        if match:
            method, return_type = match.groups()
            line = '{} {};\n'.format(return_type, method)

        # handle extensions (adding inheritance to ease model navigation)
        match = re.search('^\s*extend\s+(.+?)\s*\{', line)
        if match:
            fname = os.path.splitext(os.path.basename(filename))[0] + str(line_nr)
            line = 'class Extend_{0}_{1} extends {1} {{\n'.format(fname, match.group(1))

        # add public to the outermost block
        if block_level <= 1:
            match = re.search('^\s*((?:interface|enum|class).*)', line)
            if match:
                line = 'public ' + match.group(1) + '\n'

        output += line

    print(output, end='')



if __name__ == '__main__':
    for filename in sys.argv[1:]:
        handle_file(filename)
