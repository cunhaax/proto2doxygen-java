PROJECTS = {
	'foo-api': {
		'repo': 'ssh://git@my.domain.com/myprojects/foo-api.git',
		'branch': 'master',
		'proto_dir': 'foo/src/main/resources/protobuf'
	},
	'bar-catalog': {
		'repo': 'ssh://git@my.domain.com/myprojects/bar-catalog.git',
		'branch': 'master',
		'proto_dir': 'src/main/resources'
	},
}

WORK_DIR = '/tmp/proto2doxygen-java'
