#!/usr/bin/env python3

"""Automated protobuf to Doxygen generator"""

__author__ = "André Cunha"
__license__ = "GPLv3"



from os import makedirs, path
import re
from subprocess import Popen, PIPE, run, STDOUT

import settings

SCRIPT_DIR = path.dirname(path.abspath(__file__))


def checkout(target_dir, repo, branch, **kwargs):
	print(target_dir, repo, branch)
	run(['git', 'clone', repo, target_dir])
	run(['git', 'checkout', branch], cwd=target_dir)


def gen_doxyfile(src_dir, name, **kwargs):
	with open('Doxyfile', 'r') as doxy:
		for line in doxy:
			res = line
			match = re.search('^INPUT\s*=.*', line)
			if match:
				res = 'INPUT=' + src_dir

			match = re.search('^PROJECT_NAME\s*=.*', line)
			if match:
				res = 'PROJECT_NAME=' + '\"{}\"'.format(name)

			match = re.search('^OUTPUT_DIRECTORY\s*=.*', line)
			if match:
				out_dir = path.join(SCRIPT_DIR, 'html',name)
				print('creating dir:', out_dir)
				makedirs(out_dir)
				res = 'OUTPUT_DIRECTORY=' + out_dir

			yield res.encode()


def exec_doxygen(target_dir, name, proto_dir, **kwargs):
	try:
		proc = Popen(['doxygen', '-'], stdout=PIPE, stdin=PIPE, stderr=STDOUT)
		for b in gen_doxyfile(path.join(target_dir, proto_dir), name):
			try:
				proc.stdin.write(b)
			except BrokenPipeError:
				print(proc.stdout.read().decode(), end='')
				break
	finally:
		proc.stdin.close()
		print(proc.stdout.read().decode(), end='')
		proc.stdout.close()
		proc.wait()


def main():
	run(['rm', '-rf', settings.WORK_DIR])
	run(['rm', '-rf', path.join(SCRIPT_DIR, 'html')])
	for k, v in settings.PROJECTS.items():
		target_dir = path.join(settings.WORK_DIR, k)
		checkout(target_dir, **v)
		exec_doxygen(target_dir, k, **v)


if __name__ == '__main__':
	main()
