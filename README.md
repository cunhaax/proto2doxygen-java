# Features #

These scripts provide automated protobuf to Doxygen (java) documentation.

The following conversions are done by the filter **proto2java.py**:

* message --> class
* required --> java comment
* optional --> java comment
* value types --> according with the official documentation: https://developers.google.com/protocol-buffers/docs/proto#scalar
* oneof test_oneof --> class OneOf_test_oneof aggregating all the oneof attributes
* service --> interface
* extend EndPoint --> class Extend_Filename.proto_EndPoint extends EndPoint


The **run.py** script reads the git repositories configured in **settings.py** and generates the documentation for each one.


# Requirements #

1. Python3
2. Doxygen >= 1.8.13
3. graphviz

# Support #

Tested with proto2


# How to run #

## Automatically for multiple repositories ##

You must have ssh keys configured for the repos

1. Adjust the **settings.py** according to your needs
2. Execute the script `./run.py` to generate the documentation
3. Throw the html files in **html** directory to a Nginx or Apache httpd (or simply use `python3 -m http.server` cmd)


## Manually ##

1. Adjust the **Doxyfile** according to your needs (you might want to change `INPUT`, `PROJECT_NAME` and `OUTPUT_DIRECTORY`)
2. Run `doxygen` command
